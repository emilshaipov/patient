package ru.ser.clinic;

public class Main {
    public static void main(String[] args) {
        Patient patient1 = new Patient("Петрунин", 1995, 30, true);
        Patient patient2 = new Patient("Шаипов", 2002, 31, true);
        Patient patient3 = new Patient("Попов", 1989, 32, false);
        Patient patient4 = new Patient("Карпинский", 2008, 33, false);
        Patient patient5 = new Patient("Андреев", 2000, 34, true);

        Patient[] patients = {patient1, patient2, patient3, patient4, patient5};

        infOfPatient(patients);
    }

    /**
     * Метод проверки диспансеризации пациентов рожденных до 2000 года
     *
     * @param patients - пациенты
     */
    private static void infOfPatient(Patient[] patients) {
        for (Patient patient : patients) {
            if (patient.isDis() && patient.getNumberOfBorn() < 2000) {
                System.out.println(patient.getSurname() + " - Прошел диспансеризацию \n " + "Информация о пациенте: " + patient.toString());
            }
        }
    }
}